package br.com.alura.chain.descontos.inter;

import br.com.alura.chain.descontos.model.Orcamento;

public interface Desconto {

	double desconta(Orcamento orcamento);
	void setProximo(Desconto proximo);
}
