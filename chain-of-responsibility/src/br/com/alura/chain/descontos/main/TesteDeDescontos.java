package br.com.alura.chain.descontos.main;

import br.com.alura.chain.descontos.model.Item;
import br.com.alura.chain.descontos.model.Orcamento;
import br.com.alura.chain.descontos.negocio.CalculadorDeDescontos;

public class TesteDeDescontos {

	public static void main(String[] args) {
		CalculadorDeDescontos descontos = new CalculadorDeDescontos();
		Orcamento orcamento = new Orcamento(501.0);

		orcamento.adicionaItem(new Item("Caneta", 250.0));
		orcamento.adicionaItem(new Item("Lapís", 250.0));

		double descontoFinal = descontos.calcula(orcamento);

		System.out.println(descontoFinal);
	}

}
