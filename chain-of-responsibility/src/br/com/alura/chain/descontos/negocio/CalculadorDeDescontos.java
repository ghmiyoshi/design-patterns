package br.com.alura.chain.descontos.negocio;

import br.com.alura.chain.descontos.impl.DescontoPorCincoItens;
import br.com.alura.chain.descontos.impl.DescontoPorMaisDeQuinhentosReais;
import br.com.alura.chain.descontos.impl.DescontoPorVendaCasada;
import br.com.alura.chain.descontos.impl.SemDesconto;
import br.com.alura.chain.descontos.model.Orcamento;

public class CalculadorDeDescontos {

	public double calcula(Orcamento orcamento) {
		DescontoPorCincoItens d1 = new DescontoPorCincoItens();
		DescontoPorMaisDeQuinhentosReais d2 = new DescontoPorMaisDeQuinhentosReais();
		DescontoPorVendaCasada d3 = new DescontoPorVendaCasada();
		SemDesconto d4 = new SemDesconto();

		d1.setProximo(d2);
		d2.setProximo(d3);
		d3.setProximo(d4);

		return d1.desconta(orcamento);
	}
}
