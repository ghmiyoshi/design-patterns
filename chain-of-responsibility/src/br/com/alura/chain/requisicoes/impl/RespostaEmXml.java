package br.com.alura.chain.requisicoes.impl;

import br.com.alura.chain.requisicoes.formatos.Formato;
import br.com.alura.chain.requisicoes.inter.Resposta;
import br.com.alura.chain.requisicoes.model.Conta;
import br.com.alura.chain.requisicoes.model.Requisicao;

public class RespostaEmXml implements Resposta {
	
	private Resposta outraResposta;

	@Override
	public void responde(Requisicao requisicao, Conta conta) {
		if(requisicao.getFormato() == Formato.XML) System.out.println("<conta><titular>" + conta.getNome() + "</titular><saldo>" + conta.getSaldo() + "</saldo></conta>");
		else outraResposta.responde(requisicao, conta);
	}

	@Override
	public void setProxima(Resposta resposta) {
		this.outraResposta = resposta;
	}

}
