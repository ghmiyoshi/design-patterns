package br.com.alura.chain.requisicoes.main;

import br.com.alura.chain.requisicoes.formatos.Formato;
import br.com.alura.chain.requisicoes.impl.RespostaEmCsv;
import br.com.alura.chain.requisicoes.impl.RespostaEmXml;
import br.com.alura.chain.requisicoes.model.Conta;
import br.com.alura.chain.requisicoes.model.Requisicao;

public class TesteRequisicoes {

	public static void main(String[] args) {
		RespostaEmCsv respostaEmCsv = new RespostaEmCsv();
		RespostaEmXml respostaEmXml = new RespostaEmXml();

		Conta conta = new Conta("Gabriel", 500.0);
		Requisicao requisicao = new Requisicao(Formato.XML);
		Requisicao requisicao2 = new Requisicao(Formato.CSV);

		respostaEmCsv.responde(requisicao2, conta);
		respostaEmXml.responde(requisicao, conta);
	}

}
