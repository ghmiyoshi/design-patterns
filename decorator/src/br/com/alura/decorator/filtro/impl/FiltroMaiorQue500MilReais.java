package br.com.alura.decorator.filtro.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.alura.decorator.filtro.abs.Filtro;
import br.com.alura.decorator.filtro.model.Conta;

public class FiltroMaiorQue500MilReais extends Filtro {

	public FiltroMaiorQue500MilReais(Filtro outroFiltro) {
		super(outroFiltro);
	}

	public FiltroMaiorQue500MilReais() {
	}

	@Override
	public List<Conta> filtra(List<Conta> contas) {
		List<Conta> contasFiltradas = new ArrayList<>();

		for (Conta conta : contas) {
			if (conta.getValor() > 500000.0) {
				contasFiltradas.add(conta);
				System.out.println("Contas com saldo maior que R$ 500.000,00: " + conta.getTitular());
			}
		}

		contasFiltradas.addAll(proximo(contas));

		return contasFiltradas;
	}

}
