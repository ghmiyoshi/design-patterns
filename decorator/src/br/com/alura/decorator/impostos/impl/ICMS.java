package br.com.alura.decorator.impostos.impl;

import br.com.alura.decorator.impostos.inter.Imposto;
import br.com.alura.decorator.impostos.model.Orcamento;

public class ICMS extends Imposto {

	public ICMS(Imposto outroImposto) {
		super(outroImposto);
	}

	public ICMS() {

	}

	@Override
	public double calcula(Orcamento orcamento) {
		return orcamento.getValor() * 0.1 + calculoDoOutroImposto(orcamento);
	}

}
