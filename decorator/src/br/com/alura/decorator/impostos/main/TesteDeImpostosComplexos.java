package br.com.alura.decorator.impostos.main;

import br.com.alura.decorator.impostos.impl.ICCC;
import br.com.alura.decorator.impostos.impl.ICMS;
import br.com.alura.decorator.impostos.impl.ISS;
import br.com.alura.decorator.impostos.model.Orcamento;

public class TesteDeImpostosComplexos {

	public static void main(String[] args) {
		ISS iss = new ISS(new ICMS(new ICCC()));
		
		Orcamento orcamento = new Orcamento(500);
		double valor = iss.calcula(orcamento);
		
		System.out.println(valor);
		
	}

}
