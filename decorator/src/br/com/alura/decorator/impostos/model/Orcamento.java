package br.com.alura.decorator.impostos.model;

public class Orcamento {

	private double valor;

	public Orcamento(double valor) {
		this.valor = valor;
	}

	public double getValor() {
		return valor;
	}

}
