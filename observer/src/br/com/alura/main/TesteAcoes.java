package br.com.alura.main;

import java.time.LocalDate;

import br.com.alura.builder.NotaFiscalBuilder;
import br.com.alura.model.ItemDaNota;
import br.com.alura.model.NotaFiscal;
import br.com.alura.observer.EnviadorDeEmail;
import br.com.alura.observer.EnviadorDeSms;
import br.com.alura.observer.Impressora;
import br.com.alura.observer.NotaFiscalDao;

public class TesteAcoes {

	public static void main(String[] args) {
		NotaFiscalBuilder builder = new NotaFiscalBuilder()
				.adicionaAcao(new EnviadorDeEmail())
				.adicionaAcao(new NotaFiscalDao())
				.adicionaAcao(new EnviadorDeSms())
				.adicionaAcao(new Impressora());
		
		NotaFiscal nf = builder
				.paraEmpresa("Caelum")
				.comCnpj("123")
				.com(new ItemDaNota("nome", 100))
				.comObservacoes("abs")
				.naData(LocalDate.now()).constroi();
		
		System.out.println(nf.getValorBruto());
		
	}

}
