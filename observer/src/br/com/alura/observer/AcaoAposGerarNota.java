package br.com.alura.observer;

import br.com.alura.model.NotaFiscal;

public interface AcaoAposGerarNota {

	void executa(NotaFiscal nf);

}
