package br.com.alura.observer;

import br.com.alura.model.NotaFiscal;

public class Impressora implements AcaoAposGerarNota {

	@Override
	public void executa(NotaFiscal nf) {
		System.out.println("Nota impressa");
	}

}
