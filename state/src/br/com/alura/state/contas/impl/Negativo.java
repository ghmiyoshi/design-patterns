package br.com.alura.state.contas.impl;

import br.com.alura.state.contas.inter.EstadoDaConta;
import br.com.alura.state.contas.model.Conta;

public class Negativo implements EstadoDaConta {

	@Override
	public void saca(Conta conta, double valor) {
		throw new RuntimeException("Conta com saldo negativo, impossível sacar...");
	}

	@Override
	public void deposita(Conta conta, double valor) {
		double valorDepositado = valor * 0.95;
		double saldo = conta.getSaldo() + valorDepositado;
		
		conta.setSaldo(saldo);
		
		if (conta.getSaldo() > 0) conta.setEstadoAtual(new Positivo());
	}

}
