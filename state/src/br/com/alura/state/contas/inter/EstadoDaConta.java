package br.com.alura.state.contas.inter;

import br.com.alura.state.contas.model.Conta;

public interface EstadoDaConta {

	void saca(Conta conta, double valor);

	void deposita(Conta conta, double valor);

}
