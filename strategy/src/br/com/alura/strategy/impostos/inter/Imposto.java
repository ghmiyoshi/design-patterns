package br.com.alura.strategy.impostos.inter;

import br.com.alura.strategy.impostos.model.Orcamento;

public interface Imposto {

	double calcula(Orcamento orcamento);

}
