package br.com.alura.strategy.investimentos.impl;

import java.util.Random;

import br.com.alura.strategy.investimentos.inter.Investimento;
import br.com.alura.strategy.investimentos.model.Conta;

public class Moderado implements Investimento {

	private Random random;

	public Moderado() {
		this.random = new Random();
	}

	@Override
	public double calculaInvestimento(Conta conta) {
		if (random.nextInt(2) == 0)
			return conta.getSaldo() * 0.025;
		else
			return conta.getSaldo() * 0.007;
	}

}
