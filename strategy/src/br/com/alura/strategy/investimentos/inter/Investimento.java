package br.com.alura.strategy.investimentos.inter;

import br.com.alura.strategy.investimentos.model.Conta;

public interface Investimento {

	double calculaInvestimento(Conta conta);

}
