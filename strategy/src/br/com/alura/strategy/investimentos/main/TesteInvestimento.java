package br.com.alura.strategy.investimentos.main;

import br.com.alura.strategy.investimentos.impl.Arrojado;
import br.com.alura.strategy.investimentos.model.Conta;
import br.com.alura.strategy.investimentos.negocio.RealizadorDeInvestimentos;

public class TesteInvestimento {

	public static void main(String[] args) {
		Conta conta = new Conta(500);
		Arrojado arrojado = new Arrojado();
		RealizadorDeInvestimentos realizadorDeInvestimentos = new RealizadorDeInvestimentos();
		
		arrojado.calculaInvestimento(conta);
		realizadorDeInvestimentos.realizaInvestimento(conta, arrojado);
		
		
	}

}
