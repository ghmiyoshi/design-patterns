package br.com.alura.strategy.impostos.impl;

import br.com.alura.strategy.impostos.model.Item;
import br.com.alura.strategy.impostos.model.Orcamento;
import br.com.alura.strategy.impostos.negocio.TemplateDeImpostoCondicional;

public class IKCV extends TemplateDeImpostoCondicional {

	private boolean temItemMaiorQue100ReaisNo(Orcamento orcamento) {
		boolean result = false;

		for (Item item : orcamento.getItens()) {
			result = (item.getValor() > 100) ? true : false;

		}

		return result;
	}

	@Override
	protected double minimaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * 0.06;
	}

	@Override
	protected double maximaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * 0.1;
	}

	@Override
	protected boolean deveUsarMaximaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() > 500 && temItemMaiorQue100ReaisNo(orcamento);
	}

}
