package br.com.alura.strategy.impostos.model;

import java.util.List;

public class Orcamento {

	private List<Item> itens;
	private double valor;

	public Orcamento(double valor) {
		this.valor = valor;
	}

	public double getValor() {
		return valor;
	}
	
	public List<Item> getItens() {
		return itens;
	}

}
