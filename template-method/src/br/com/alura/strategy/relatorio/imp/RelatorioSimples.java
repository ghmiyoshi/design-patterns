package br.com.alura.strategy.relatorio.imp;

import br.com.alura.strategy.relatorio.negocio.TemplateRelatorio;

public class RelatorioSimples extends TemplateRelatorio {

	@Override
	protected void cabecalho() {
		System.out.println("Banco ZYS");
	}

	@Override
	protected void corpo() {

	}

	@Override
	protected void rodape() {
		System.out.println("4748-3064");
	}

}
