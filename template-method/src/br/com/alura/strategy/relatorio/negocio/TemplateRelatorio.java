package br.com.alura.strategy.relatorio.negocio;

import br.com.alura.strategy.relatorio.inter.Relatorio;

public abstract class TemplateRelatorio implements Relatorio {

	@Override
	public void gera() {
		cabecalho();
		corpo();
		rodape();
	}

	protected abstract void cabecalho();

	protected abstract void corpo();

	protected abstract void rodape();

}
